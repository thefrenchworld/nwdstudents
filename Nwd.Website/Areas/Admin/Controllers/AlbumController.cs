﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Nwd.Website.Models;
using Nwd.BackOffice.Impl;
using Nwd.BackOffice.Model;

namespace Nwd.Website.Areas.Admin.Controllers
{
    [Authorize]
    public class AlbumController : Controller
    {
        public AlbumController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AlbumController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Admin/Album/List
        public ActionResult List(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            AlbumRepository repo = new AlbumRepository();
            List<Album> albums = repo.GetAllAlbums();
            return View(albums);
        }
        //
        // GET: /Admin/Album/Edit/id
        public ActionResult Edit(int id)
        {
            AlbumRepository repo = new AlbumRepository();
            Album album = repo.GetAlbumForEdit(id);
            return View(album);
        }

        //
        // GET: /Admin/Album/Create
        public ActionResult Create()
        {
            return View(new Album());
        }

        //
        // GET: /Admin/Album/Delete/id
        public ActionResult Delete(int id)
        {
            AlbumRepository repo = new AlbumRepository();
            repo.DeleteAlbum(id);
            
            return View();
        }

        //
        // POST: /Admin/Album/Update
        public ActionResult Update(Album album)
        {
            AlbumRepository repo = new AlbumRepository();

            repo.EditAlbum(this.Server, album);
            return View();
        }

        //
        // POST: /Admin/Album/Save
        public ActionResult Save(Album album)
        {
            AlbumRepository repo = new AlbumRepository();
            album.Id = repo.GetAllAlbums().Max(a => a.Id) + 1;
            repo.CreateAlbum(album, this.Server);
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }


    }
}