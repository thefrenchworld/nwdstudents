﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nwd.Website.Startup))]
namespace Nwd.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
